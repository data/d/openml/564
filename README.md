# OpenML dataset: fried

https://www.openml.org/d/564

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This is an artificial data set used in Friedman (1991) and also
described in Breiman (1996,p.139). The cases are generated using the
following method: Generate the values of 10 attributes, X1, ..., X10
independently each of which uniformly distributed over [0,1]. Obtain
the value of the target variable Y using the equation:

Y = 10 * sin(pi * X1 * X2) + 20 * (X3 - 0.5)^2 + 10 * X4 + 5 * X5 + sigma(0,1)

Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
Original source: Breiman (1996, p.139).
Characteristics: 40768 cases, 11 continuous attributes

References

BREIMAN, L. (1996): Bagging Predictors. Machine Learning, 24(3), 123--140. Kluwer Academic Publishers.
FRIEDMAN, J. (1991): Multivariate Adaptative Regression Splines. Annals of Statistics, 19:1, 1--141.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/564) of an [OpenML dataset](https://www.openml.org/d/564). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/564/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/564/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/564/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

